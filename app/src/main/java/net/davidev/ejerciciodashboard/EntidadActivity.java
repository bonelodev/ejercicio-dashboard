package net.davidev.ejerciciodashboard;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Locale;

public class EntidadActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entidad);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.help:
//                Intent intent = new Intent(this, ManualActivity.class);
//                startActivity(intent);
                break;
            case R.id.exit:
                AlertDialog.Builder message = new AlertDialog.Builder(this);
                message.setMessage(R.string.exitmsg);
                message.setTitle(R.string.quit);
                message.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                message.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog dialog = message.create();
                dialog.show();
                break;
            case R.id.about:
//                setFragment(6);
                break;
            case R.id.lang_eng:
                Locale localizacion = new Locale("en","");

                Locale.setDefault(localizacion);
                Configuration config = new Configuration();
                config.locale = localizacion;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                Intent refrescar = new Intent(this, MainActivity.class);
                startActivity(refrescar);
                finish();
                break;
            case R.id.lang_spa:
                Locale localizacion2 = new Locale("es","");

                Locale.setDefault(localizacion2);
                Configuration config2 = new Configuration();
                config2.locale = localizacion2;
                getBaseContext().getResources().updateConfiguration(config2, getBaseContext().getResources().getDisplayMetrics());
                Intent refrescar2 = new Intent(this, MainActivity.class);
                startActivity(refrescar2);
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
