package net.davidev.ejerciciodashboard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

import net.davidev.ejerciciodashboard.Fragmets.MainFragment;

public class MainActivity extends AppCompatActivity {

    Fragment mainFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainFragment = new MainFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragments, mainFragment).commit();


    }
}
